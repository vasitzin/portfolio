import adapter from "@sveltejs/adapter-static";

/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    adapter: adapter({
      pages: "public",
      assets: "public",
      fallback: "/index.html",
    }),
  },
  vite: {},
};

export default config;
