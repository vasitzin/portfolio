import { readable, writable } from "svelte/store";
import constructionPng from "$lib/assets/construction.png";
import poolancePng from "$lib/assets/poolance.png";
import zoomerdbPng from "$lib/assets/zoomerdb.png";
import maskittyPng from "$lib/assets/maskitty.png";

function toggleDrawerMobile() {
  const { subscribe, set } = writable({
    drawerActive: "home",
    activeType: "home",
    showcaseUrl: "",
    showcaseImage: "",
  });

  return {
    subscribe,
    toggle: (obj) => set(obj),
  };
}

// DESKTOP

// MOBILE
export const drawerMobile = toggleDrawerMobile();

export const isMobile = writable("");
export const email = "vasileios.tzinis@tutanota.com";
export const matrix = "@elafaros";
export const linkedin = "www.linkedin.com/in/vasileiostzinis";
export const gitlab = "gitlab.com/users/vasitzin/projects";
export const github = "github.com/vasitzin";
export const skills = readable(["C++", "Rust", "Java", "Python", "SvelteJS", "NodeJS"]);
export const repo = readable("https://gitlab.com/vasitzin/portfolio");
export const license = readable("http://creativecommons.org/licenses/by/4.0");
export const licensePng = readable("https://i.creativecommons.org/l/by/4.0/80x15.png");
export const urls = readable({
  Timedash: "https://timedash.eu",
  RAT: "https://risk-analysis-tool.com",
  Poolance: "https://poolance-crypto-monitor.web.app",
  ZoomerDB: "https://gitlab.com/vasitzin/zoomerdb",
  maskitty: "https://gitlab.com/vasitzin/maskitty",
  Schedulator: "https://gitlab.com/vasitzin/schedulator",
});
export const imageUrls = readable({
  Timedash: "https://timedash.eu/assets/img/emp-dash.webp",
  RAT: "https://risk-analysis-tool.com/assets/img/Promo-organization.png",
  Poolance: poolancePng,
  ZoomerDB: zoomerdbPng,
  maskitty: maskittyPng,
  Schedulator: constructionPng,
});
