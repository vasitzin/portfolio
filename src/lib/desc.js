export default {
  web: {
    Timedash:`
      <q>Timedash is a tool that allows employers to monitor each employee's performance. Each employee can log his hours for each project he's assigned to. After each month the employer can then asses if the employee's logs are satisfactory where he can either validate or dispute the employee's timesheet.</q><br /><br />
      The area I worked on on Timedash was the backend. I was responsible for managing our <b>Git</b> repository, the <b>MongoDB</b> database, the <b>Node.js</b> server and a request api for the frontend that was built with <b>Angular</b>.
      Technologies I worked with:<ul>
      <li>Mocha Supertest for testing the api,</li>
      <li>Firebase,</li>
      <li>Express.js,</li>
      <li>MongoDB,</li>
      <li>Stripe and</li>
      <li>Nodemailer.</li>
      </ul>
      `,
    RAT:`
      <q>RAT was developed with the purpose of helping the user assess risks. By accurately visualizing each risk's importance based on the user's input, RAT helps the user take action earlier.</q><br /><br />
      In this project my role was that of the backend developer. It was my first professional work, as a freelancer 
       and my first time using Node.js and Javascript in general. 
      Other technologies I worked with:<ul>
      <li>Express.js,</li>
      <li>Firebase,</li>
      <li>Stripe and</li>
      <li>Nodemailer.</li>
      </ul>
      `,
    Poolance:`
      <q>Poolance is a website where you can check your mining pool balances from a single place.</q><br /><br />
      This was the first website I made on my own mostly because I grew tired of 'double-clicking-and-ctrl+c'ing to copy my balance from 2miners, plus I wanted to learn frontend for some time. 
      It's made with Svelte and fetches information from crypto pool platforms like 2miners and Minexmr and prices from Coinbase. 
      Technologies I used:<ul>
      <li>Firebase,</li>
      <li>Express.js and</li>
      <li>MongoDB</li>
      </ul><br/>Not all the features are completed yet...
      `,
  },
  progs: {
    ZoomerDB:`
      ZoomerDB is a general purpose database for home users where they can 
      store any data from keeping track of tv shows to daily expenses.<br/>
      It's still early in development but the core features and the gui are functional.
      Get the demo <a rel="noopener noreferrer" target="_blank" href="https://gitlab.com/vasitzin/zoomerdb/-/releases/1.3.3">
      here</a>.
      `,
    maskitty:`
      Maskitty is small terminal utility that generates hexadecimal cpu affinity masks.<br/>
      The bitmask is generated using the cpu cores provided by the user. 
      Programs use this hexadecimal number, which specifies which cores should be utilized, 
      usually for optimization purposes. This project is made with C++ using CMake/GCC. 
      For educational purposes I've also ported it to 
      '<a target=_blank rel=noopener noreferrer href=https://gitlab.com/vasitzin/maskitty-rs>Rust</a>, '
      '<a target=_blank rel=noopener noreferrer href=https://gitlab.com/vasitzin/maskitty-lua>Lua</a> and '
      '<a target=_blank rel=noopener noreferrer href=https://gitlab.com/vasitzin/maskitty-py>Python</a>'
      `,
    Schedulator:`
      Schedulator is a desktop application which aims to help 
      bussinesses generate their schedules fast and fairly for their employees.<br/>
      It is written in Java and the gui is made with JavaFX/FXML. 
      Although the gui is still WIP and more advanced features are not implemented yet 
      the core functionality is accessible from the terminal through it's cli interface.
      `,
  },
};
